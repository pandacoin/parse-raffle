const input = document.getElementById('addresses');
const amount = document.getElementById('amount');
const chunksize = document.getElementById('chunksize');
const output = document.getElementById('output');

function update () {
  const amount = parseInt(document.getElementById('amount').value) || 1000;
  const chunksize = parseInt(document.getElementById('chunksize').value) || 250;
  const targets = document.getElementById('addresses').value.split(/\r?\n/)
      .map(line => line.trim())
      .filter(line => line.length);
  const invalid = targets.reduce((all, line, i) => {
    try {
      BitcoinJS.address.toOutputScript(line);
    } catch (e) {
      all.push(`Line ${i+1}: ${line}`);
    }
    return all;
  }, []);
  if (invalid.length)
    return output.innerHTML = `<div style="color: red"><br><h3>Invalid inputs:</h3>${invalid.join('<br>')}</div>`;
  const it = targets.reduce((all, one, i) => {
    const ch = Math.floor(i/chunksize);
    all[ch] = [].concat((all[ch] || []), one);
    return all;
  }, []);
  const txBuilders = it.reduce((all, arr) => {
    const txBuilder = new BitcoinJS.TransactionBuilder();
    txBuilder.setTime(Math.floor(new Date().getTime() / 1000));
    arr.forEach((address) => {
      txBuilder.addOutput(address, amount * 1000000);
    });
    all.push(txBuilder);
    return all;
  }, []);
  let html = '<h1>Transactions</h1>Backcheck on <a target="_blank" href="https://pandacoin.gitlab.io/cointoolkit/?mode=pandacoin#verify">Cointoolkit</a><br><br>';
  txBuilders.forEach((txBuilder, i) => {
    const hex = txBuilder.buildIncomplete().toHex();
    html += `Chunk ${i+1}<br><textarea>${hex}</textarea><br>`;
  });
  output.innerHTML = html;
}

input.addEventListener('input', update);
amount.addEventListener('input', update);
chunksize.addEventListener('input', update);
